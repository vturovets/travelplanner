package com.turovets.model;

import java.util.Date;

/**
 * Created by Admin on 22.10.2016.
 */
public abstract class AbstractEntry {
    protected long id;
    protected Date createdDate;
    protected Date updatedDate;

    public AbstractEntry() {
        this.createdDate = new Date();
    }

    protected String getName(){
        StringBuilder sb = new StringBuilder();
        sb.append("No name has been specified.").append(" ").append(this.toString());
        return sb.toString();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("AbstractEntity{id=").append(id).append(", created on ")
                .append(createdDate).append(", updated on ").append(updatedDate).append("}");
        return sb.toString();
    }
}
